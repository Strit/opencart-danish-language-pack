# Opencart 3.0 Danish Language Pack

## Work in Progress

Full danish translation for all catalog and admin pages in OpenCart 3.0.

Some pages will not get translated first time around, these include specific shipping methods and payment methods.


## Installation instructions
To install the language pack please follow these steps.

1. Download the source, by clicking the download button on this repository.
2. Unpack the compressed archive. This should result in 2 folders (catalog and admin) and this readme file.
3. Copy the da-dk folders from `catalog/languages` and `admin/languages` to the same folders on your Opencart server.
4. Open up your servers admin console (example: exampleshop.com/admin) and log in.
5. Go to System -> Localisation -> Languages. Click the + in the upper right corner to add a new language. Fill in Code: da-dk, name: Dansk and locale: da-DK,da_DK.UTF-8,da_DK,danish. Make sure it's enabled and Save.
6. Go to System -> Settings and edit the Store. Under Local, you should now be able to switch Language and Admin Language to Dansk.
