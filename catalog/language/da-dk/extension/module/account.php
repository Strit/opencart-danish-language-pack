<?php
// Heading
$_['heading_title']    = 'Konto';

// Text
$_['text_register']    = 'Registrér';
$_['text_login']       = 'Log Ind';
$_['text_logout']      = 'Log Ud';
$_['text_forgotten']   = 'Glemt Adgangskode';
$_['text_account']     = 'Min Konto';
$_['text_edit']        = 'Ændre Konto';
$_['text_password']    = 'Adgangskode';
$_['text_address']     = 'Adressebog';
$_['text_wishlist']    = 'Ønskeliste';
$_['text_order']       = 'Ordre Historik';
$_['text_download']    = 'Hentninger';
$_['text_reward']      = 'Point';
$_['text_return']      = 'Returneringer';
$_['text_transaction'] = 'Transaktioner';
$_['text_newsletter']  = 'Nyhedsbrev';
$_['text_recurring']   = 'Tilbagevendende betalinger';
