<?php
// PhotoSwipe
$_['button_share']					= 'Del';
$_['button_pswp_close']				= 'Luk (Esc)';
$_['button_pswp_toggle_fullscreen']	= 'Fuldskærm';
$_['button_pswp_zoom']				= 'Zoom ind/ud';
$_['button_pswp_prev']				= 'Forrige (venstre pil)';
$_['button_pswp_next']				= 'Næste (højre pil)';
