<?php
// Entry
$_['entry_name']			= 'Hvad er dit navn';
$_['entry_telephone']		= 'Dit telefonnummer';
$_['entry_enquiry']			= 'Kommentar';
$_['entry_calltime']		= 'Bedste tid til et opkald';

// Text
$_['text_agree']			= 'Jeg har læst og er enig i <a href="%s" class="agree"><b>%s</b></a>';

// Error
$_['error_telephone']		= 'Telefonnummer skal være mellem 4 og 24 tegn!';
$_['error_name']			= 'Navnet skal være mellem 3 og 32 tegn!';
$_['error_enquiry']			= 'Kommentaren skal være mellem 10 og 360 tegn!';
$_['error_calltime']		= 'Specificér det bedste tid til opkaldet!';
$_['error_agree']			= 'Advarsel: Du skal være enig i %s!';
