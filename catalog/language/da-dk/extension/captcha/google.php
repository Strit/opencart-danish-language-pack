<?php
// Text
$_['text_captcha']  = 'Captcha';

// Entry
$_['entry_captcha'] = 'Venligst færdiggør captcha verifikationen herunder';

// Error
$_['error_captcha'] = 'Verifikationen er ikke korrekt!';
