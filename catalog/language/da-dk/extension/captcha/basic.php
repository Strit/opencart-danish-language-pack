<?php
// Text
$_['text_captcha']  = 'Captcha';

// Entry
$_['entry_captcha'] = 'Skriv koden i boksen herunder';

// Error
$_['error_captcha'] = 'Verificeringskode passer ikke med billedet!';
