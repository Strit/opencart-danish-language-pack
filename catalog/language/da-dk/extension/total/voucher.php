<?php
// Heading
$_['heading_title'] = 'Brug Gavekort';

// Text
$_['text_voucher']  = 'Gavekort (%s)';
$_['text_success']  = 'Success: Dit gavekort er blevet angivet!';

// Entry
$_['entry_voucher'] = 'Indtast din gavekortskode her';

// Error
$_['error_voucher'] = 'Advarsel: Gavekortet er enten ikke gyldig eller beløbet er brugt op!';
$_['error_empty']   = 'Advarsel: Venligst indtast gavekortskoden!';
