<?php
// Heading
$_['heading_title']        = 'Estimeret Levering Og Moms';

// Text
$_['text_success']         = 'Success: Dit leveringsestimat er blevet angivet!';
$_['text_shipping']        = 'Indtast destinationen for at få et leveringsestimat.';
$_['text_shipping_method'] = 'Venligst vælg din foretrukne leveringsmetode til denne ordre.';

// Entry
$_['entry_country']        = 'Land';
$_['entry_zone']           = 'Region / Stat';
$_['entry_postcode']       = 'Postnummer';

// Error
$_['error_postcode']       = 'Postnummer skal være mellem 2 og 10 tegn!';
$_['error_country']        = 'Vælg venligst et land!';
$_['error_zone']           = 'Vælg venligst en region / stat!';
$_['error_shipping']       = 'Advarsel: Leveringsmetode påkrævet!';
$_['error_no_shipping']    = 'Advarsel: Ingen leveringsmuligheder tilgængelige. Venligst <a href="%s">kontakt os</a> for hjælp!';
