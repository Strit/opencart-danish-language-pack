<?php
// Heading
$_['heading_title'] = 'Brug Point (Tilgængelig %s)';

// Text
$_['text_reward']   = 'Point (%s)';
$_['text_order_id'] = 'Ordre ID: #%s';
$_['text_success']  = 'Success: Dine point rabat er blevet angivet!';

// Entry
$_['entry_reward']  = 'Point at bruge (Max %s)';

// Error
$_['error_reward']  = 'Advarsel: Venligst indtast mængden af popint der skal bruges!';
$_['error_points']  = 'Advarsel: Du har ikke %s point!';
$_['error_maximum'] = 'Advarsel: Det maksimale antal popint der kan angives er %s!';
