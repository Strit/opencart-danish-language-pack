<?php
// Heading
$_['heading_title'] = 'Brug Kupon Kode';

// Text
$_['text_coupon']   = 'Kupon (%s)';
$_['text_success']  = 'Success: Din kupon er blevet angivet!';

// Entry
$_['entry_coupon']  = 'Skriv din kupon her';

// Error
$_['error_coupon']  = 'Advarsel: Kupon er enten ikke gyldig, udløbet eller er nået dens brugsgrænse!';
$_['error_empty']   = 'Advarsel: Venligst skriv en kupon kode!';
