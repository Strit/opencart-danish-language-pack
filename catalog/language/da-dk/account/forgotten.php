<?php
// Heading
$_['heading_title']   = 'Glemt Din Adgangskode?';

// Text
$_['text_account']    = 'Konto';
$_['text_forgotten']  = 'Glemt Adgangskode';
$_['text_your_email'] = 'Din Email Adresse';
$_['text_email']      = 'Skriv email adressen der er forbundet med din konto. Klik indsend for at få et reset link sendt til din email adresse.';
$_['text_success']    = 'En email med bekræftelses link er blevet tilsendt din email adresse.';

// Entry
$_['entry_email']     = 'Email Adresse';
$_['entry_password']  = 'Ny Adgangskode';
$_['entry_confirm']   = 'Bekræft';

// Error
$_['error_email']     = 'Advarsel: Email adressen er ikke i vores arkiv, prøv venligst igen!';
$_['error_approved']  = 'Advarsel: Din konto kræver godkendelse før du kan logge ind.';
$_['error_password']  = 'Adgangskoden skal være mellem 4 og 20 tegn!';
$_['error_confirm']   = 'Adgangskoden og adgangskode bekræftelsen passer ikke sammen!';
