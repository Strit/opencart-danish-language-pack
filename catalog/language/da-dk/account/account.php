<?php
// Heading
$_['heading_title']       = 'Min Konto';

// Text
$_['text_account']        = 'Konto';
$_['text_my_account']     = 'Min Konto';
$_['text_my_orders']      = 'Mine Ordrer';
$_['text_my_affiliate']   = 'Min Affiliate Konto';
$_['text_my_newsletter']  = 'Nyhedsbrev';
$_['text_edit']           = 'Redigér dine konto oplysninger';
$_['text_password']       = 'Ændre din adgangskode';
$_['text_address']        = 'Ændre dine adressebog';
$_['text_credit_card']    = 'Håndtér Gemte Kredit Kort';
$_['text_wishlist']       = 'Ændre din ønskeliste';
$_['text_order']          = 'Se din ordre historik';
$_['text_download']       = 'Hentninger';
$_['text_reward']         = 'Dine Point';
$_['text_return']         = 'Se dine returneringsforespørgsler';
$_['text_transaction']    = 'Dine Transaktioner';
$_['text_newsletter']     = 'Tilmeld / afmeld nyhedsbrevet';
$_['text_recurring']      = 'Tilbagevendende betalinger';
$_['text_transactions']   = 'Transaktioner';
$_['text_affiliate_add']  = 'Registrér en affiliate konto';
$_['text_affiliate_edit'] = 'Redigér dine affiliate oplysninger';
$_['text_tracking']       = 'Brugerdefinreret Affiliate Sporingskode';
?>
