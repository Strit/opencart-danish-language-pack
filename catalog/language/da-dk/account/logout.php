<?php
// Heading
$_['heading_title'] = 'Konto Log Ud';

// Text
$_['text_message']  = '<p>Du er blevet logget ud af din konto. Det er nu sikkert at forlade computeren.</p><p>Din indkøbskurv er blevet gemt, tingene i den bliver genskabt nåt du logger ind på din konto igen.</p>';
$_['text_account']  = 'Konto';
$_['text_logout']   = 'Log Ud';
