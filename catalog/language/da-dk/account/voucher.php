<?php
// Heading
$_['heading_title']    = 'Køb et Gavekort';

// Text
$_['text_account']     = 'Konto';
$_['text_voucher']     = 'Gavekort';
$_['text_description'] = 'Dette gavekort vil blive sendt i en email til modtageren efter din ordre er blevet betalt.';
$_['text_agree']       = 'Jeg forstår at gavekort ikke kan refunderes.';
$_['text_message']     = '<p>Tak fordi du købte et gavekort! Når din ordre er færdiggjort vil gavekort modtageren få det tilsendt i en email med detaljer om hvordan den indløses.</p>';
$_['text_for']         = '%s Gavekort til %s';

// Entry
$_['entry_to_name']    = 'Modtagers Navn';
$_['entry_to_email']   = 'Modtagers Email';
$_['entry_from_name']  = 'Dit Navn';
$_['entry_from_email'] = 'Din Email';
$_['entry_theme']      = 'Gavekortets Tema';
$_['entry_message']    = 'Besked';
$_['entry_amount']     = 'Beløb';

// Help
$_['help_message']     = 'Valgfri';
$_['help_amount']      = 'Værdi skal være mellem %s og %s';

// Error
$_['error_to_name']    = 'Modtagers Navn skal være mellem 1 og 64 tegn!';
$_['error_from_name']  = 'Dit Navn skal være mellem 1 og 64 tegn!';
$_['error_email']      = 'Email adressen ser ikke ud til at være gyldig!';
$_['error_theme']      = 'Du skal vælge et tema!';
$_['error_amount']     = 'Beløbet skal være mellem %s og %s!';
$_['error_agree']      = 'Advarsel: Du skal være enig i at gavekort ikke kan refunderes!';
