<?php
// Heading
$_['heading_title']      = 'Dine Transaktioner';

// Column
$_['column_date_added']  = 'Dato Tilføjet';
$_['column_description'] = 'Beskrivelse';
$_['column_amount']      = 'Mængde (%s)';

// Text
$_['text_account']       = 'Konto';
$_['text_transaction']   = 'Dine Transaktioner';
$_['text_total']         = 'Din nuværende balance er:';
$_['text_empty']         = 'Du har ikke nogen transaktioner!';
