<?php
// Heading
$_['heading_title']      = 'Produkt Returneringer';

// Text
$_['text_account']       = 'Konto';
$_['text_return']        = 'Returneringsoplysninger';
$_['text_return_detail'] = 'Returneringsdetaljer';
$_['text_description']   = 'Udfyld venligst formularen herunder for at få et RMA nummer.';
$_['text_order']         = 'Ordre Oplysninger';
$_['text_product']       = 'Produkt Oplysninger';
$_['text_reason']        = 'Grund til returnering';
$_['text_message']       = '<p>For fordi du indsendte din returneringsforespørgsel. Din forespørgsel er blevet sendt til den relevante afdeling.</p><p> Du vil bliver notificeret via email når med status opdateringer på din forespørgsel.</p>';
$_['text_return_id']     = 'Returnering ID:';
$_['text_order_id']      = 'Ordre ID:';
$_['text_date_ordered']  = 'Ordre Dato:';
$_['text_status']        = 'Status:';
$_['text_date_added']    = 'Dato Tilføjet:';
$_['text_comment']       = 'Returneringskommentarer';
$_['text_history']       = 'Returneringshistorik';
$_['text_empty']         = 'Du har endnu ikke returneret nogle varer!';
$_['text_agree']         = 'Jeg har læst og er enig i <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'Returnering ID';
$_['column_order_id']    = 'Ordre ID';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Dato Tilføjet';
$_['column_customer']    = 'Kunde';
$_['column_product']     = 'Produkt Navn';
$_['column_model']       = 'Model';
$_['column_quantity']    = 'Antal';
$_['column_price']       = 'Pris';
$_['column_opened']      = 'Åbnet';
$_['column_comment']     = 'Kommentar';
$_['column_reason']      = 'Grund';
$_['column_action']      = 'Handling';

// Entry
$_['entry_order_id']     = 'Ordre ID';
$_['entry_date_ordered'] = 'Ordre Dato';
$_['entry_firstname']    = 'Fornavn';
$_['entry_lastname']     = 'Efternavn';
$_['entry_email']        = 'Email';
$_['entry_telephone']    = 'Telefonnummer';
$_['entry_product']      = 'Producḱt Navn';
$_['entry_model']        = 'Produkt Kode';
$_['entry_quantity']     = 'Antal';
$_['entry_reason']       = 'Grund til returnering';
$_['entry_opened']       = 'Produkt er åbnet';
$_['entry_fault_detail'] = 'Fejl eller andre detaljer';

// Error
$_['text_error']         = 'Returneringen du efterspurgte kunne ikke findes!';
$_['error_order_id']     = 'Ordre ID påkrævet!';
$_['error_firstname']    = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']     = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_email']        = 'Email adressen ser ikke ud til at være gyldig!';
$_['error_telephone']    = 'Telefonnummer skal være mellem 3 og 32 tegn!';
$_['error_product']      = 'Produkt Navn skal være længere end 3 og mindre end 255 tegn!';
$_['error_model']        = 'Produkt Model skal være længere end 3 og mindre end 64 tegn!';
$_['error_reason']       = 'Du skal vælge en returneringsgrund!';
$_['error_agree']        = 'Advarsel: Du skal være enig i %s!';
