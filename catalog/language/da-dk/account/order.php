<?php
// Heading
$_['heading_title']         = 'Ordre Historik';

// Text
$_['text_account']          = 'Konto';
$_['text_order']            = 'Ordre Oplysninger';
$_['text_order_detail']     = 'Ordre Detaljer';
$_['text_invoice_no']       = 'Faktura Nr.:';
$_['text_order_id']         = 'Ordre ID:';
$_['text_date_added']       = 'Dato Tilføjet:';
$_['text_shipping_address'] = 'Leveringsadresse';
$_['text_shipping_method']  = 'Leveringsmetode:';
$_['text_payment_address']  = 'Betalingsadresse';
$_['text_payment_method']   = 'Betalingsmetode:';
$_['text_comment']          = 'Ordre Kommentarer';
$_['text_history']          = 'Ordre Historik';
$_['text_success']          = 'Success: Du har tilføjet <a href="%s">%s</a> til din <a href="%s">indkøbskurv</a>!';
$_['text_empty']            = 'Du har ikke lavet nogle ordre endnu!';
$_['text_error']            = 'Ordren du har forespurgt kunne ikke findes!';

// Column
$_['column_order_id']       = 'Ordre ID';
$_['column_customer']       = 'Kunde';
$_['column_product']        = 'Antal Produkter';
$_['column_name']           = 'Produkt Navn';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Antal';
$_['column_price']          = 'Pris';
$_['column_total']          = 'Total';
$_['column_action']         = 'Handling';
$_['column_date_added']     = 'Dato Tilføjet';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Kommentar';

// Error
$_['error_reorder']         = '%s kan ikke genbestilles på nuværende tidspunkt.';
