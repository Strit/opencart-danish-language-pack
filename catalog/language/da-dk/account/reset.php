<?php
// Heading
$_['heading_title']  = 'Nulstil din adgangskode';

// Text
$_['text_account']   = 'Konto';
$_['text_password']  = 'Indtast den ønskede adgangskode.';
$_['text_success']   = 'Success: Din adgangskode er blevet opdateret.';

// Entry
$_['entry_password'] = 'Adgangskode';
$_['entry_confirm']  = 'Bekræft';

// Error
$_['error_password'] = 'Adgangskoden skal være mellem 4 og 20 tegn!';
$_['error_confirm']  = 'Adgangskoden og adgangskode bekræftelsen er ikke ens!';
$_['error_code']     = 'Adgangskode nulstillingsjoden er ugyldig eller har været brugt før!';
