<?php
// Heading
$_['heading_title']     = 'Konto Hentninger';

// Text
$_['text_account']      = 'Konto';
$_['text_downloads']    = 'Hentninger';
$_['text_empty']        = 'Du har ikke lavet nogle hentbare ordre!';

// Column
$_['column_order_id']   = 'Ordre ID';
$_['column_name']       = 'Navn';
$_['column_size']       = 'Størrelse';
$_['column_date_added'] = 'Dato Tilføjet';
