<?php
// Heading
$_['heading_title']                = 'Konto Log Ind';

// Text
$_['text_account']                 = 'Konto';
$_['text_login']                   = 'Log Ind';
$_['text_new_customer']            = 'Ny Kunde';
$_['text_register']                = 'Registrér Konto';
$_['text_register_account']        = 'Ved at lave en konto vil du k unne handle hurtigere, være opdateret på ordrer status, og holde styr på dine tidligere ordrer.';
$_['text_returning_customer']      = 'Eksisterende Kunde';
$_['text_i_am_returning_customer'] = 'Jeg er en eksisterende kunde';
$_['text_forgotten']               = 'Glemt Adgangskode';

// Entry
$_['entry_email']                  = 'Email Adresse';
$_['entry_password']               = 'Adgangskode';

// Error
$_['error_login']                  = 'Advarsel: Email adresse og/eller adgangskode ikke genkendt.';
$_['error_attempts']               = 'Advarsel: Din konto har overskredet antallet af log ind forsøg. Prøv venligst igen om 1 time.';
$_['error_approved']               = 'Advarsel: Din konto kræver godkendelse før du kan logge ind.';
