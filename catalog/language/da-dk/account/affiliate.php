<?php
// Heading
$_['heading_title']             = 'Dine Affiliate Oplysninger';

// Text
$_['text_account']              = 'Konto';
$_['text_affiliate']            = 'Affiliate';
$_['text_my_affiliate']         = 'Min Affiliate Konto';
$_['text_payment']              = 'Betalingsoplysninger';
$_['text_cheque']               = 'Check';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Overførsel';
$_['text_success']              = 'Success: Din konto er blevet opdateret.';
$_['text_agree']                = 'Jeg har løst og er enig i <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_company']             = 'Firma';
$_['entry_website']             = 'Hjemmeside';
$_['entry_tax']                 = 'Moms ID';
$_['entry_payment']             = 'Betalingsmetode';
$_['entry_cheque']              = 'Check Udskriver Navn';
$_['entry_paypal']              = 'PayPal Email Konto';
$_['entry_bank_name']           = 'Bank Navn';
$_['entry_bank_branch_number']  = 'ABA/BSB nummer (Branch Nummer)';
$_['entry_bank_swift_code']     = 'SWIFT Kode';
$_['entry_bank_account_name']   = 'Konto Navn';
$_['entry_bank_account_number'] = 'Konto Nummer';

// Error
$_['error_agree']               = 'Advarsel: Du skal være enig i %s!';
$_['error_cheque']              = 'Check Udskriver Navn påkrævet!';
$_['error_paypal']              = 'PayPal Email Adressen ser ikke ud til at være gyldig!';
$_['error_bank_account_name']   = 'Konto Navn påkrævet!';
$_['error_bank_account_number'] = 'Konto Nummer påkrævet!';
$_['error_custom_field']        = '%s påkrævet!';
