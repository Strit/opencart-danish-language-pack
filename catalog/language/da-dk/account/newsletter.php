<?php
// Heading
$_['heading_title']    = 'Nyhedsbrev Abonnement';

// Text
$_['text_account']     = 'Konto';
$_['text_newsletter']  = 'Nyhedsbrev';
$_['text_success']     = 'Success: Dit nyhedsbrevsabonnement er blevet opdateret!';

// Entry
$_['entry_newsletter'] = 'Abonnér';
