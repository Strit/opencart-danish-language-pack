<?php
// Heading
$_['heading_title'] = 'Din Konto Er Oprettet!';

// Text
$_['text_message']  = '<p>Tillykke! Din nye konto er blevet oprettet!</p> <p>Du kan gøre brug af medlemsfordelene for at forbedre din handle oplevelse hos os.</p> <p>Hvis du har nogen spørgsmål omkring driften af denne online shop, så skriv venligst en email til butiksejeren. Hvis du ikke har fået den indenfor en time, så <a href="%s">kontakt os</a> venligst.</p>';
$_['text_approval'] = '<p>Tak fordi du registrerede hos %s!</p><p>Du vil blive notificeret via email når din konto er blevet aktiveret af butiksejeren.</p><p>Hvis du har nogen spørgsmål omkring driften af denne online shop, venligst <a href="%s">kontakt butiksejeren</a>.</p>';
$_['text_account']  = 'Konto';
$_['text_success']  = 'Success';
