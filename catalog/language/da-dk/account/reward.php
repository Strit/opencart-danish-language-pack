<?php
// Heading
$_['heading_title']      = 'Dine Point';

// Column
$_['column_date_added']  = 'Dato Tilføjet';
$_['column_description'] = 'Beskrivelse';
$_['column_points']      = 'Point';

// Text
$_['text_account']       = 'Konto';
$_['text_reward']        = 'Point';
$_['text_total']         = 'Dine samlede antal point er:';
$_['text_empty']         = 'Du har ikke nok point!';
