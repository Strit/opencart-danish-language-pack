<?php
// Heading
$_['heading_title']    = 'Affiliate Sporing';

// Text
$_['text_account']     = 'Konto';
$_['text_description'] = 'For at sikre at du bliver betalt for referancer du sender til os, bliver vi nødt til at kunne spore dem ved at placere en sporingskode i en URL\'er der linker til os. Du kan bruge værktøjerne herunder for at genere links til hjemmesiden.';

// Entry
$_['entry_code']       = 'Din Springskode';
$_['entry_generator']  = 'Sporingslink Generator';
$_['entry_link']       = 'Sproringslink';

// Help
$_['help_generator']  = 'Skriv navnet på det produkt du gerne vil linke til';
