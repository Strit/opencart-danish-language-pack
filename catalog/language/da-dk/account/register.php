<?php
// Heading
$_['heading_title']        = 'Registrér Konto';

// Text
$_['text_account']         = 'Konto';
$_['text_register']        = 'Registrér';
$_['text_account_already'] = 'Hvis du allerede har en konto hos os, så venligst log ind på <a href="%s">log ind siden</a>.';
$_['text_your_details']    = 'Dine Personlige Detaljer';
$_['text_newsletter']      = 'Nyhedsbrev';
$_['text_your_password']   = 'Din Adgangskode';
$_['text_agree']           = 'Jeg har læst og er enig i <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Kunde Gruppe';
$_['entry_firstname']      = 'Fornavn';
$_['entry_lastname']       = 'Efternavn';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Telefonnummer';
$_['entry_newsletter']     = 'Abonnér';
$_['entry_password']       = 'Adgangskode';
$_['entry_confirm']        = 'Bekræft Adgangskode';

// Error
$_['error_exists']         = 'Advarsel: Email adressen er allerede registreret!';
$_['error_firstname']      = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']       = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_email']          = 'Email adressen ser ikke ud til at være gyldig!';
$_['error_telephone']      = 'Telefonnumer skal være mellem 3 og 32 tegn!';
$_['error_custom_field']   = '%s påkrævet!';
$_['error_password']       = 'Adgangskode skal være mellem 4 og 20 tegn!';
$_['error_confirm']        = 'Adgangskode bekræftelsen passer ikke med adgangskoden!';
$_['error_agree']          = 'Advarsel: Du skal være enig i %s!';
