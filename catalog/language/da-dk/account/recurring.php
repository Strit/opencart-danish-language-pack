<?php
// Heading
$_['heading_title']                        = 'Tilbagevendende Betalinger';

// Text
$_['text_account']                         = 'Konto';
$_['text_recurring']                       = 'Tilbagevendende Betalingsoplysninger';
$_['text_recurring_detail']                = 'Tilbagevendende Betalingsdetaljer';
$_['text_order_recurring_id']              = 'Tilbagevendende ID:';
$_['text_date_added']                      = 'Dato Tilføjet:';
$_['text_status']                          = 'Status:';
$_['text_payment_method']                  = 'Betalingsmetode:';
$_['text_order_id']                        = 'Ordre ID:';
$_['text_product']                         = 'Produkt:';
$_['text_quantity']                        = 'Antal:';
$_['text_description']                     = 'Beskrivelse';
$_['text_reference']                       = 'Referance';
$_['text_transaction']                     = 'Transaktioner';
$_['text_status_1']                        = 'Aktiv';
$_['text_status_2']                        = 'Ikke Aktiv';
$_['text_status_3']                        = 'Annulleret';
$_['text_status_4']                        = 'Suspenderet';
$_['text_status_5']                        = 'Udløbet';
$_['text_status_6']                        = 'Ventende';
$_['text_transaction_date_added']          = 'Oprettet';
$_['text_transaction_payment']             = 'Betaling';
$_['text_transaction_outstanding_payment'] = 'Udestående betaling';
$_['text_transaction_skipped']             = 'Betaling sprunget over';
$_['text_transaction_failed']              = 'betaling fejlet';
$_['text_transaction_cancelled']           = 'Annulleret';
$_['text_transaction_suspended']           = 'Suspenderet';
$_['text_transaction_suspended_failed']    = 'Suspenderet fra fejlet betaling';
$_['text_transaction_outstanding_failed']  = 'Udestående betaling fejlet';
$_['text_transaction_expired']             = 'Udløbet';
$_['text_empty']                           = 'Ingen tilbagevendende betalinger fundet!';
$_['text_error']                           = 'Den tilbagevendende ordre du efterspurgte kunne ikke findes!';
$_['text_cancelled']                       = 'Tilbagevendende betaling er blevet annulleret';

// Column
$_['column_date_added']                    = 'Dato Tilføjet';
$_['column_type']                          = 'Type';
$_['column_amount']                        = 'Mængde';
$_['column_status']                        = 'Status';
$_['column_product']                       = 'Produkt';
$_['column_order_recurring_id']            = 'Tilbagevendende ID';

// Error
$_['error_not_cancelled']                  = 'Fejl: %s';
$_['error_not_found']                      = 'Kunne ikke annullere tilbagevendende';

// Button
$_['button_return']                        = 'Tilbage';
