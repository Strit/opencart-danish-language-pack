<?php
// Heading
$_['heading_title']      = 'Adressebog';

// Text
$_['text_account']       = 'Konto';
$_['text_address_book']  = 'Adressebog';
$_['text_address_add']   = 'Tilføj Adresse';
$_['text_address_edit']  = 'Redigér Adresse';
$_['text_add']           = 'Din adresse er blevet tilføjet';
$_['text_edit']          = 'Din adresse er blevet opdateret';
$_['text_delete']        = 'Din adresse er blevet slettet';
$_['text_empty']         = 'Du har ingen adresse på din konto.';

// Entry
$_['entry_firstname']    = 'Fornavn';
$_['entry_lastname']     = 'Efternavn';
$_['entry_company']      = 'Firma';
$_['entry_address_1']    = 'Adresse 1';
$_['entry_address_2']    = 'Adresse 2';
$_['entry_postcode']     = 'Postnummer';
$_['entry_city']         = 'By';
$_['entry_country']      = 'Land';
$_['entry_zone']         = 'Region / Stat';
$_['entry_default']      = 'Standard Adresse';

// Error
$_['error_delete']       = 'Advarsel: Du skal have mindst én adresse!';
$_['error_default']      = 'Advarsel: Du kan ikke slette din standard adresse!';
$_['error_firstname']    = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']     = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_address_1']    = 'Adresse skal være mellem 3 og 128 tegn!';
$_['error_postcode']     = 'Postnummer skal være mellem 2 og 10 tegn!';
$_['error_city']         = 'By skal være mellem 2 og 128 tegn!';
$_['error_country']      = 'Vælg venligst et land!';
$_['error_zone']         = 'Vælg venligst en region / stat!';
$_['error_custom_field'] = '%s påkrævet!';
