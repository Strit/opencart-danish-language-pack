<?php
// Text
$_['text_upload']    = 'Din fil er blevet uploadet!';

// Error
$_['error_filename'] = 'Filnavn skal være mellem 3 og 64 tegn!';
$_['error_filetype'] = 'Ugyldig filtype!';
$_['error_upload']   = 'Upload påkrævet!';
