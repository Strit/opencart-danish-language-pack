<?php
// Heading
$_['heading_title'] = 'Din Affiliate Konto Er Oprettet!';

// Text
$_['text_message']  = '<p>Tillykke! Din nye konto er blevet oprettet!</p> <p>Du er nu medlem af %s affiliaterne.</p> <p>Hvis du har nogen spørgsmål omkring driften af affiliate systemet, så send en email til butiksejeren.</p> <p>En bekræftelse er blevet sendt til den givne email adresse. Hvis du ikke har modtaget inden for en time, så <a href="%s">kontakt os</a>.</p>';
$_['text_approval'] = '<p>Tak fordi du oprettede en affiliate konto hos %s!</p><p>Du vil blive notificeret via email når din konto er blevet aktiveret af butiksejeren.</p><p>Hvis du har nogen spørgsmål til driften af affiliate systemet, så <a href="%s">kontakt butiksejeren</a>.</p>';
$_['text_account']  = 'Konto';
$_['text_success']  = 'Success';
