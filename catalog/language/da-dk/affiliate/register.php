<?php
// Heading
$_['heading_title']             = 'Affiliate Program';

// Text
$_['text_account']              = 'Konto';
$_['text_register']             = 'Registrér Affiliate';
$_['text_account_already']      = 'Hvis du allerede har en konto hos os, så kan du logge ind på <a href="%s">log ind siden</a>.';
$_['text_signup']               = 'For at oprette en affiliate konto skal du udfylde formularen herunder med de krævede felter:';
$_['text_your_details']         = 'Dine Personlige Detaljer';
$_['text_your_address']         = 'Dine Adresse Detaljer';
$_['text_your_affiliate']       = 'Dine Affiliate Oplysninger';
$_['text_your_password']        = 'Din Adgangskode';
$_['text_cheque']               = 'Check';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Overførsel';
$_['text_agree']                = 'Jeg har læst og er enig i <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group']      = 'Kunde Gruppe';
$_['entry_firstname']           = 'Fornavn';
$_['entry_lastname']            = 'Efternavn';
$_['entry_email']               = 'Email';
$_['entry_telephone']           = 'Telefonnummer';
$_['entry_company']             = 'Firma';
$_['entry_website']             = 'Hjemmeside';
$_['entry_tax']                 = 'Moms ID';
$_['entry_payment']             = 'Betalingsmetode';
$_['entry_cheque']              = 'Check Betaler Navn';
$_['entry_paypal']              = 'PayPal Email Konto';
$_['entry_bank_name']           = 'Bank Navn';
$_['entry_bank_branch_number']  = 'ABA/BSB nummer (Branch Nummer)';
$_['entry_bank_swift_code']     = 'SWIFT Kode';
$_['entry_bank_account_name']   = 'Konto Navn';
$_['entry_bank_account_number'] = 'Konto Nummer';
$_['entry_password']            = 'Adgangskode';
$_['entry_confirm']             = 'Bekræft Adgangskode';

// Error
$_['error_exists']              = 'Advarsel: Email adressen er allerede registreret!';
$_['error_firstname']           = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']            = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_email']               = 'Email adressen ser ikke ud til at være gyldig!';
$_['error_telephone']           = 'Telefonnummer skal være nellem 3 og 32 tegn!';
$_['error_custom_field']        = '%s påkrævet!';
$_['error_cheque']              = 'Check Betaler Navn påkrævet!';
$_['error_paypal']              = 'PayPal Email adresse ser ikke ud til at være gyldig!';
$_['error_bank_account_name']   = 'Konto Navn påkrævet!';
$_['error_bank_account_number'] = 'Konto Nummer påkrævet!';
$_['error_password']            = 'Adgangskoden skal være ellem 4 og 20 tegn!';
$_['error_confirm']             = 'Adgangskode bekræftelsen stemmer ikke overens med adgangskoden!';
$_['error_agree']               = 'Advarsel: Du skal være enig i %s!';
