<?php
// Heading
$_['heading_title']                 = 'Affiliate Program';

// Text
$_['text_account']                  = 'Konto';
$_['text_login']                    = 'Log Ind';
$_['text_description']              = '<p>%s affiliate program er gratis og giver medlemmer mulighed for at tjeken lidt ved at placere links på deres hjememside som reklamere for %s eller specifikke produkter. Ethvert salg lavet af kunder der har klikket et sådan link vil give affiliate fortjeneste. Standard fortjeneste raten er nu %s.</p><p>For flere oplysninger, besøg vores FAQ side eller se vores Affiliate retningslinjer.</p>';
$_['text_new_affiliate']            = 'Ny Affiliate';
$_['text_register_account']         = '<p>Jeg er ikke en nuværende affiliate.</p><p>Klik Fortsæt herunder for at oprette en ny affiliate konto. Bemærk at dette ikke er forbundet til din kunde konto.</p>';
$_['text_returning_affiliate']      = 'Affiliate Log Ind';
$_['text_i_am_returning_affiliate'] = 'Jeg er en eksisterende affiliate.';
$_['text_forgotten']                = 'Glemt Adgangskode';

// Entry
$_['entry_email']                   = 'Affiliate Email';
$_['entry_password']                = 'Adgangskode';

// Error
$_['error_login']                   = 'Advarsel: Email og/eller Adgangskode er ikke fundet i vores system.';
$_['error_attempts']                = 'Advarsel: Din konto har overtrådt antallet af log ind forsøg. Prøv venligst igen om en time.';
$_['error_approved']                = 'Advarsel: Din konto kræver godkendelse før du kan logge ind.';
