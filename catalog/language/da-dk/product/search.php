<?php
// Heading
$_['heading_title']     = 'Søg';
$_['heading_tag']       = 'Tag - ';

// Text
$_['text_search']       = 'Produkter der møder søgekriterierne';
$_['text_keyword']      = 'Nøgleord';
$_['text_category']     = 'Alle Kategorier';
$_['text_sub_category'] = 'Søg i underkategorier';
$_['text_empty']        = 'Der er ingen produlter der passer til søgekriterierne.';
$_['text_quantity']     = 'Antal:';
$_['text_manufacturer'] = 'Fanbrikant:';
$_['text_model']        = 'Produkt Kode:';
$_['text_points']       = 'Point:';
$_['text_price']        = 'Pris:';
$_['text_tax']          = 'Eksl Moms:';
$_['text_reviews']      = 'Baseret på %s anmeldelser.';
$_['text_compare']      = 'Produkt Sammenligning (%s)';
$_['text_sort']         = 'Sortér Efter:';
$_['text_default']      = 'Standard';
$_['text_name_asc']     = 'Navn (A - Å)';
$_['text_name_desc']    = 'Navn (Å - A)';
$_['text_price_asc']    = 'Pris (Lav &gt; Høj)';
$_['text_price_desc']   = 'Pris (Høj &gt; Lav)';
$_['text_rating_asc']   = 'Bedømmelse (Lavest)';
$_['text_rating_desc']  = 'Bedømmelse (Højest)';
$_['text_model_asc']    = 'Model (A - Å)';
$_['text_model_desc']   = 'Model (Å - A)';
$_['text_limit']        = 'Vis:';

// Entry
$_['entry_search']      = 'Søgekriterier';
$_['entry_description'] = 'Søg i produktbeskrivelser';
