<?php
// Heading
$_['heading_title']     = 'Find Din Favorit Fabrikant';

// Text
$_['text_brand']        = 'Fabrikant';
$_['text_index']        = 'Fabrikant Indeks:';
$_['text_error']        = 'Fabrikant ikke fundet!';
$_['text_empty']        = 'Der er ingen produkter at vise.';
$_['text_quantity']     = 'Antal:';
$_['text_manufacturer'] = 'Fabrikant:';
$_['text_model']        = 'Produkt Kode:';
$_['text_points']       = 'Point:';
$_['text_price']        = 'Pris:';
$_['text_tax']          = 'Eksl Moms:';
$_['text_compare']      = 'Produkt Sammenligning (%s)';
$_['text_sort']         = 'Sortér Efter:';
$_['text_default']      = 'Standard';
$_['text_name_asc']     = 'Navn (A - Å)';
$_['text_name_desc']    = 'Navn (Å - A)';
$_['text_price_asc']    = 'Pris (Lav &gt; Høj)';
$_['text_price_desc']   = 'Pris (Høj &gt; Lav)';
$_['text_rating_asc']   = 'Bedømmelse (Lavest)';
$_['text_rating_desc']  = 'Bedømmelse (Højest)';
$_['text_model_asc']    = 'Model (A - Å)';
$_['text_model_desc']   = 'Model (Å - A)';
$_['text_limit']        = 'Vis:';
