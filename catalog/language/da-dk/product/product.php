<?php
// Text
$_['text_search']              = 'Søg';
$_['text_brand']               = 'Fabrikant';
$_['text_manufacturer']        = 'Fabrikant:';
$_['text_model']               = 'Produkt Kode:';
$_['text_reward']              = 'Point:';
$_['text_points']              = 'Pris i point:';
$_['text_stock']               = 'Tilgængelighed:';
$_['text_instock']             = 'På Lager';
$_['text_tax']                 = 'Eksl Moms:';
$_['text_discount']            = ' eller mere ';
$_['text_option']              = 'Tilgængelige Muligheder';
$_['text_minimum']             = 'Dette produkt har en minimum mængde på %s';
$_['text_reviews']             = '%s anmeldelser';
$_['text_write']               = 'skriv en anmeldelse';
$_['text_login']               = 'venligst <a href="%s">log ind</a> eller <a href="%s">registrér</a> for at anmelde';
$_['text_no_reviews']          = 'Der er ingen anmeldelser af dette produkt.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML er ikke oversat!';
$_['text_success']             = 'Tak for din anmeldelse. Den er blevet sendt til godkendelse hos webmasteren.';
$_['text_related']             = 'Relaterede Produkter';
$_['text_tags']                = 'Tags:';
$_['text_error']               = 'Produkt ikke fundet!';
$_['text_payment_recurring']   = 'Betalingsprofil';
$_['text_trial_description']   = '%s hver %d %s(s) over %d betaling(er) så';
$_['text_payment_description'] = '%s hver %d %s(s) over %d betaling(er)';
$_['text_payment_cancel']      = '%s hver %d %s(s) indtil annulleret';
$_['text_day']                 = 'dag';
$_['text_week']                = 'uge';
$_['text_semi_month']          = 'halv-måned';
$_['text_month']               = 'måned';
$_['text_year']                = 'år';

// Entry
$_['entry_qty']                = 'Antal';
$_['entry_name']               = 'Dit Navn';
$_['entry_review']             = 'Din Anmeldelse';
$_['entry_rating']             = 'Bedømmelse';
$_['entry_good']               = 'God';
$_['entry_bad']                = 'Dårlig';

// Tabs
$_['tab_description']          = 'Beskrivelse';
$_['tab_attribute']            = 'Specifikation';
$_['tab_review']               = 'Anmeldelser (%s)';

// Error
$_['error_name']               = 'Advarsel: Anmelder Navn skal være mellem 3 og 25 tegn!';
$_['error_text']               = 'Advarsel: Anmeldelse skal være mellem 25 og 1000 tegn!';
$_['error_rating']             = 'Advarsel: Vælg venligst en bedømmelse!';
