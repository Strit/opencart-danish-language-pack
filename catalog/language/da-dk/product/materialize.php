<?php
$_['text_sku']						= 'SKU';
$_['text_upc']						= 'UPC';
$_['text_ean']						= 'EAN';
$_['text_jan']						= 'JAN';
$_['text_isbn']						= 'ISBN';
$_['text_mpn']						= 'MPN';
$_['text_location']					= 'Lokation';
$_['text_dimension']				= 'Dimensioner (L x B x H)';
$_['text_weight']					= 'Vægt';
$_['text_remainder']				= 'Tilbage';
$_['text_size_chart']				= 'Størrelsestabel';
$_['text_category']					= 'Kategori';
$_['text_bonus_points']				= 'Point for køb';
$_['text_end_promotion']			= 'Tilbuddet slutter den';
$_['text_percent']					= 'Rabat';
$_['text_more_detailed']			= 'Flere detaljer';

// Product list
$_['text_searched']					= 'Du søgte efter:';
$_['text_refine']					= 'Udvidet kategori';
$_['text_sort_short']				= 'Sortér';
$_['entry_instock']					= 'Vis kun på lager';
$_['text_manufacturers_starting']	= 'Alle fabrikanter der starter med';
$_['text_view_all_products']		= 'Vis alle produkter';
