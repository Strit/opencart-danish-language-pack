<?php
// Heading
$_['heading_title']  = 'Kontakt Os';

// Text
$_['text_location']  = 'Vores Lokation';
$_['text_store']     = 'Vores Butik';
$_['text_contact']   = 'Kontakt Formular';
$_['text_address']   = 'Adresse';
$_['text_telephone'] = 'Telefonnummer';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Åbningstider';
$_['text_comment']   = 'Kommentarer';
$_['text_success']   = '<p>Din forespørgsel er blevet sendt til butiksejeren!</p>';

// Entry
$_['entry_name']     = 'Dit Navn';
$_['entry_email']    = 'Email Adresse';
$_['entry_enquiry']  = 'Forespørgsel';

// Email
$_['email_subject']  = 'Forespørgsel %s';

// Errors
$_['error_name']     = 'Navn skal være mellem 3 og 32 tegn!';
$_['error_email']    = 'Email adressen ser ikke ud til at være gyldig!';
$_['error_enquiry']  = 'Forespørgsel skal være mellem 10 og 3000 tegn!';
