<?php
// Heading
$_['heading_title']    = 'Site Map';

// Text
$_['text_special']     = 'Tilbud';
$_['text_account']     = 'Min Konto';
$_['text_edit']        = 'Konto Oplysninger';
$_['text_password']    = 'Adgangskode';
$_['text_address']     = 'Adressebog';
$_['text_history']     = 'Ordre Historik';
$_['text_download']    = 'Hentninger';
$_['text_cart']        = 'Indkøbskurv';
$_['text_checkout']    = 'Check Ud';
$_['text_search']      = 'Søg';
$_['text_information'] = 'Oplysninger';
$_['text_contact']     = 'Kontakt Os';
