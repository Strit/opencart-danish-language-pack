<?php
// Text
$_['text_information']  = 'Oplysninger';
$_['text_service']      = 'Kundeservice';
$_['text_extra']        = 'Ekstra';
$_['text_contact']      = 'Kontakt Os';
$_['text_return']       = 'Returneringer';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Fabrikanter';
$_['text_voucher']      = 'Gavekort';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Tilbud';
$_['text_account']      = 'Min Konto';
$_['text_order']        = 'Ordre Historik';
$_['text_wishlist']     = 'Ønskeliste';
$_['text_newsletter']   = 'Nyhedsbrev';
$_['text_powered']      = 'Lavet Med <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
