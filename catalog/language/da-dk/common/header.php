<?php
// Text
$_['text_home']          = 'Hjem';
$_['text_wishlist']      = 'Ønskeliste (%s)';
$_['text_shopping_cart'] = 'Indkøbskurv';
$_['text_category']      = 'Kategorier';
$_['text_account']       = 'Min Konto';
$_['text_register']      = 'Registrér';
$_['text_login']         = 'Log Ind';
$_['text_order']         = 'Ordre Historik';
$_['text_transaction']   = 'Transaktioner';
$_['text_download']      = 'Hentninger';
$_['text_logout']        = 'Log Ud';
$_['text_checkout']      = 'Check Ud';
$_['text_search']        = 'Søg';
$_['text_all']           = 'Vis Alle';
