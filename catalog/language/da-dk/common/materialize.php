<?php
$_['text_blog']				= 'Blog';
$_['text_contact']			= 'Kontakt Os';
$_['text_about']			= 'Om';
$_['text_delivery']			= 'Levering';
$_['text_comparison_list']	= 'Sammenlign Produkter';
$_['text_shopping_cart']	= 'Din indkøbskurv';
$_['text_cat_says']			= 'Lad mig hjælpe dig med at bruge nogle penge :)';
$_['text_product_removed']	= 'Ting fjernet fra kurv';
$_['text_cancel']			= 'Annullér';
