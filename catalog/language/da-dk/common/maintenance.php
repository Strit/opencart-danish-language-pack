<?php
// Heading
$_['heading_title']    = 'Vedligeholdelse';

// Text
$_['text_maintenance'] = 'Vedligeholdelse';
$_['text_message']     = '<h1 style="text-align:center;">Vi er lige igang med en planlagt vedligeholdelse. <br/>Vi er tilbage så snart vi er færdige. Kom tilbage igen om lidt.</h1>';
