<?php
// Heading
$_['heading_title'] = 'Betaling Fejlede!';

// Text
$_['text_basket']   = 'Indkøbskurv';
$_['text_checkout'] = 'Check Ud';
$_['text_failure']  = 'Betaling Fejlede';
$_['text_message']  = '<p>Der opstod et problem med din betaling eller ordren blev ikke gennemført.</p>

<p>Mulige grunde kan være:</p>
<ul>
  <li>Ikke nok tilrådighed</li>
  <li>Verifikation fejlet</li>
</ul>

<p>Venligst prøv at bestil igen med en anden betalingsmetode.</p>

<p>Hvis problemet fortsætter så venligst <a href="%s">kontakt os</a> med ordre detaljerne for den ordre du prøver at lave.</p>
';
