<?php
// Heading
$_['heading_title']                  = 'Check Ud';

// Text
$_['text_cart']                      = 'Indkøbskurv';
$_['text_checkout_option']           = 'Trin %s: Check Ud Muligheder';
$_['text_checkout_account']          = 'Trin %s: Konto Og Betalingsdetaljer';
$_['text_checkout_payment_address']  = 'Trin %s: Betalingsdetaljer';
$_['text_checkout_shipping_address'] = 'Trim %s: Leveringsdetaljer';
$_['text_checkout_shipping_method']  = 'Trin %s: Leveringsmetode';
$_['text_checkout_payment_method']   = 'Trin %s: Betalingsmetode';
$_['text_checkout_confirm']          = 'Trin %s: Bekræft Ordre';
$_['text_modify']                    = 'Ændre &raquo;';
$_['text_new_customer']              = 'Ny Kunde';
$_['text_returning_customer']        = 'Eksisterende Kunde';
$_['text_checkout']                  = 'Check Ud Muligheder:';
$_['text_i_am_returning_customer']   = 'Jeg er en eksisterende kunde';
$_['text_register']                  = 'Registrér Konto';
$_['text_guest']                     = 'Gæste Check Ud';
$_['text_register_account']          = 'Ved at oprette en konto for du mulighed for at handle hurtigere, blive opdateret med en ordres status eller holde styr på tidligere ordre.';
$_['text_forgotten']                 = 'Glemt Adgangskode';
$_['text_your_details']              = 'Dine Personlige Detaljer';
$_['text_your_address']              = 'Din Adresse';
$_['text_your_password']             = 'Din Adgangskode';
$_['text_agree']                     = 'Jeg har læst og er enig i <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Jeg vil bruge en ny adresse';
$_['text_address_existing']          = 'jeg vil bruge en eksisterende adresse';
$_['text_shipping_method']           = 'Vælg venligst din foretrukne leveringsmetode til denne ordre.';
$_['text_payment_method']            = 'Vælg venligst din foretrukne betalingsmetode til denne ordre.';
$_['text_comments']                  = 'Tilføj Kommentar Til Din Ordre';
$_['text_recurring_item']            = 'Tilbagevendende Ting';
$_['text_payment_recurring']         = 'Betalingsprofil';
$_['text_trial_description']         = '%s hver %d %s(s) over %d betaling(er) så';
$_['text_payment_description']       = '%s hver %d %s(s) over %d betaling(er)';
$_['text_payment_cancel']            = '%s hver %d %s(s) indtil annulleret';
$_['text_day']                       = 'dag';
$_['text_week']                      = 'uge';
$_['text_semi_month']                = 'halv-måned';
$_['text_month']                     = 'måned';
$_['text_year']                      = 'år';

// Column
$_['column_name']                    = 'Produkt Navn';
$_['column_model']                   = 'Model';
$_['column_quantity']                = 'Antal';
$_['column_price']                   = 'Enhedspris';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'Email Adresse';
$_['entry_email']                    = 'Email';
$_['entry_password']                 = 'Adgangskode';
$_['entry_confirm']                  = 'Bekræft Adgangskode';
$_['entry_firstname']                = 'Fornavn';
$_['entry_lastname']                 = 'Efternavn';
$_['entry_telephone']                = 'Telefonnummer';
$_['entry_address']                  = 'Vælg Adresse';
$_['entry_company']                  = 'Firma';
$_['entry_customer_group']           = 'Kunde Gruppe';
$_['entry_address_1']                = 'Adresse 1';
$_['entry_address_2']                = 'Adresse 2';
$_['entry_postcode']                 = 'Postnummer';
$_['entry_city']                     = 'By';
$_['entry_country']                  = 'Land';
$_['entry_zone']                     = 'Region / Stat';
$_['entry_newsletter']               = 'Jeg vil gerne abonnere på %s nyhedsbrevet.';
$_['entry_shipping']                 = 'Min levelrings og betalingsadresse er den sammen.';

// Error
$_['error_warning']                  = 'Der var et problem med at gennemgå din ordre! Hvis problemet fortsætter så prøv en anden betalingsmetode eller kontakt butiksejeren ved at <a href="%s">klikke her</a>.';
$_['error_login']                    = 'Advarsel: Email adresse og/eller adgangskode er ikke fundet i vores system.';
$_['error_attempts']                 = 'Advarsel: Din konto har overskredet det tilladte antal loginforsøg. Prøv venligst igen om 1 time.';
$_['error_approved']                 = 'Advarsel: Din konto kræver godkendelse før du kan logge ind.';
$_['error_exists']                   = 'Advarsel: Email adressen er allerede registreret!';
$_['error_firstname']                = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']                 = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_email']                    = 'Email adressen ser ikke ud til at være gydlig!';
$_['error_telephone']                = 'Telefonnummer skal være mellem 3 og 32 tegn!';
$_['error_password']                 = 'Adgangskode skal være mellem 4 og 20 tegn!';
$_['error_confirm']                  = 'Adgangskode bekræftelsen og adgangskoden er ikke ens!';
$_['error_address_1']                = 'Adresse 1 skal være mellem 3 og 128 tegn!';
$_['error_city']                     = 'By skal være mellem 2 og 128 tegn!';
$_['error_postcode']                 = 'Postnummer skal være mellem 2 og 10 tegn!';
$_['error_country']                  = 'Vælg venligst et land!';
$_['error_zone']                     = 'Vælg venligst en region / stat!';
$_['error_agree']                    = 'Advarsel: Du skal være enig i %s!';
$_['error_address']                  = 'Advarsel: DU skal vælge en adresse!';
$_['error_shipping']                 = 'Advarsel: Leveringsmetode påkrævet!';
$_['error_no_shipping']              = 'Advarsel: Ingen leveringsmuligheder tilgængelige. Venligst <a href="%s">kontakt os</a> for hjælp!';
$_['error_payment']                  = 'Advarsel: Betalingsmetode påkrævet!';
$_['error_no_payment']               = 'Advarsel: Ingen Betalingsmuligheder tilgængelige. Venligst <a href="%s">kontakt os</a> for hjælp!';
$_['error_custom_field']             = '%s påkrævet!';
