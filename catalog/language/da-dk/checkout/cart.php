<?php
// Heading
$_['heading_title']            = 'Indkøbskurv';

// Text
$_['text_success']             = 'Success: Du har tilføjet <a href="%s">%s</a> til din <a href="%s">indkøbskurv</a>!';
$_['text_remove']              = 'Success: Du har ændret din indkøbskurv!';
$_['text_login']               = 'Bemærk: Du skal <a href="%s">logge ind</a> eller <a href="%s">oprette en konto</a> for at se priser!';
$_['text_items']               = '%s ting - %s';
$_['text_points']              = 'Point: %s';
$_['text_next']                = 'Hvad vil du så nu?';
$_['text_next_choice']         = 'Vælg om duy har en rabat kode eller om du vil bruge point eller vil have beregning levelingomkostninger.';
$_['text_empty']               = 'Din indkøbskurv er tom!';
$_['text_day']                 = 'dag';
$_['text_week']                = 'uge';
$_['text_semi_month']          = 'halve-måned';
$_['text_month']               = 'måned';
$_['text_year']                = 'år';
$_['text_trial']               = '%s hver %s %s for %s betalinger så ';
$_['text_recurring']           = '%s hver %s %s';
$_['text_payment_cancel']      = 'indtil annulleret';
$_['text_recurring_item']      = 'Tilbagevendende Ting';
$_['text_payment_recurring']   = 'Betalingsprofil';
$_['text_trial_description']   = '%s hver %d %s(s) for %d betaling(er) så';
$_['text_payment_description'] = '%s hver %d %s(s) for %d betaling(er)';
$_['text_payment_cancel']      = '%s hver %d %s(s) indtil annulleret';

// Column
$_['column_image']             = 'Billede';
$_['column_name']              = 'Produkt Navn';
$_['column_model']             = 'Model';
$_['column_quantity']          = 'Antal';
$_['column_price']             = 'Enhedspris';
$_['column_total']             = 'Total';

// Error
$_['error_stock']              = 'Produkter markeret med *** er ikke tilgængelige i den ønskede mængde eller er ikke på lager!';
$_['error_minimum']            = 'Minimum ordre mængde for %s er %s!';
$_['error_required']           = '%s påkrævet!';
$_['error_product']            = 'Advarsel: Der er ingen produkter i din kurv!';
$_['error_recurring_required'] = 'Vælg venligst en betalings tilbagevending!';
