<?php
// Heading
$_['heading_title']        = 'Din ordre er angivet!';

// Text
$_['text_basket']          = 'Indkøbskurv';
$_['text_checkout']        = 'Check Ud';
$_['text_success']         = 'Success';
$_['text_customer']        = '<p>Din ordre er blevet angivet!</p><p>Du kan se din ordre historik ved at gå til <a href="%s">min konto</a> siden og klikke på <a href="%s">historik</a>.</p><p>Hvis dit køb har en hentning, så kan du gå til konto <a href="%s">hentninger</a> siden og se dem.</p><p>Venligst skriv til <a href="%s">butiksejeren</a> med eventuelle spørgsmål.</p><p>Tak fordi du handlede online hos os!</p>';
$_['text_guest']           = '<p>Din ordre er blevet angivet!</p><p>Venligst skriv til <a href="%s">butiksejeren</a> med eventuelle spørgsmål.</p><p>Tak fordi du handlede online hos os!</p>';
