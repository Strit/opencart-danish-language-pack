<?php
// Text
$_['text_subject']      = '%s - Ordre Opdatering %s';
$_['text_order_id']     = 'Ordre ID:';
$_['text_date_added']   = 'Dato Tilføjet:';
$_['text_order_status'] = 'Din ordre er opdateret til følgende status:';
$_['text_comment']      = 'Kommentarerne til din ordre er:';
$_['text_link']         = 'For at se din ordre kan du klikke på linket herunder:';
$_['text_footer']       = 'besvar venligst denne email hvis du har nogen spørgsmål.';
