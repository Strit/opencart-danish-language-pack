<?php
// Text
$_['text_subject']  = 'Du er blevet tilsendt et gavekort fra %s';
$_['text_greeting'] = 'Tillykke, du har modtaget et gavekort til en værdi af %s';
$_['text_from']     = 'Dette gavekort er sendt til dig af %s';
$_['text_message']  = 'Med beskeden';
$_['text_redeem']   = 'For at indlæse dette gavekort skal du skrive indløsningskoden ned, som er <b>%s</b>, og så klikke på linket herunder og købe dit ønskede produkt for dette gavekort. Du kan skrive gavekortkoden i indkøbskurven før Check Ud.';
$_['text_footer']   = 'Besvar venligst denne email hvis du har nogen spørgsmål.';
