<?php
// Text
$_['text_subject']        = '%s - Tak for at du registrerede dig';
$_['text_welcome']        = 'Velkommen og tak fordi du registrerede dig hos %s!';
$_['text_login']          = 'Din konto er oprettet og du kan logge ind med din email og adgangskode ved at besøge vores hjemmeside eller følgende link:';
$_['text_approval']       = 'Din konto skal godkendes før du kan logge ind. Når den er godkendt kan du logge ind med din email og adgangskode ved at besøge vores hjemmeside eller følgende link:';
$_['text_service']        = 'Når du er logget ind, får du adgang til andre tjenester der inkluderer at gennemse tidligere ordre, printe faktura og ændre dine konto oplysninger.';
$_['text_thanks']         = 'Tak,';
$_['text_new_customer']   = 'Ny Kunde';
$_['text_signup']         = 'En ny kunde er tilmeldt:';
$_['text_customer_group'] = 'Kunde Gruppe:';
$_['text_firstname']      = 'Fornavn:';
$_['text_lastname']       = 'Efternavn:';
$_['text_email']          = 'Email:';
$_['text_telephone']      = 'Telefonnummer:';
