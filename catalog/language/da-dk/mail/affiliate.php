<?php
// Text
$_['text_subject']        = '%s - Affiliate Program';
$_['text_welcome']        = 'Tak fordi du har tilmeldt dig %s Affiliate Program!';
$_['text_login']          = 'Din konto er nu oprettet og du kan logge på med din email adresse og adgangskode ved at besøge vores hjemmeside eller følge dette link:';
$_['text_approval']       = 'Din konto skal godkendes før du kan logge ind. Når den er godkendt kan du logge ind med din email adresse og adgangskode ved at besøge vores hjemmeside eller følge dette link:';
$_['text_service']        = 'Når du er logget ind har du mulighed for at generere sporingskoder, holde styr på betalinger og ændre dine konto oplysninger.';
$_['text_thanks']         = 'Tak,';
$_['text_new_affiliate']  = 'Ny Affiliate';
$_['text_signup']         = 'En ny affiliate er tilmeldt:';
$_['text_website']        = 'Hjemmeside:';
$_['text_customer_group'] = 'Kunde Gruppe:';
$_['text_firstname']      = 'Fornavn:';
$_['text_lastname']       = 'Efternavn:';
$_['text_company']        = 'Firma:';
$_['text_email']          = 'Email:';
$_['text_telephone']      = 'Telefonnummer:';
