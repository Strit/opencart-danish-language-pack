<?php
// Text
$_['text_subject']          = '%s - Ordre %s';
$_['text_greeting']         = 'Tak for din interesse i %s produkter. Din ordre er modtaget og vil blive sat i værk så snart betalingen er blevet bekræftet.';
$_['text_link']             = 'For at se din ordre kan du klikke på linke herunder:';
$_['text_order_detail']     = 'Ordre Detaljer';
$_['text_instruction']      = 'Instruktioner';
$_['text_order_id']         = 'Ordre ID:';
$_['text_date_added']       = 'Dato Tilføjet:';
$_['text_order_status']     = 'Ordre Status:';
$_['text_payment_method']   = 'Betalingsmetode:';
$_['text_shipping_method']  = 'Leveringsmetode:';
$_['text_email']            = 'Email:';
$_['text_telephone']        = 'Telefonnummer:';
$_['text_ip']               = 'IP Adresse:';
$_['text_payment_address']  = 'Betalingsadresse';
$_['text_shipping_address'] = 'Leveringsadresse';
$_['text_products']         = 'Produkter';
$_['text_product']          = 'Produkt';
$_['text_model']            = 'Model';
$_['text_quantity']         = 'Antal';
$_['text_price']            = 'Pris';
$_['text_order_total']      = 'Ordre Totaler';
$_['text_total']            = 'Total';
$_['text_download']         = 'Når din betaling er bekræftet kan du klikke på linket herunder for at få adgang til de produkter der skal hentes:';
$_['text_comment']          = 'Kommentaren til din ordre er:';
$_['text_footer']           = 'Besvar venligst denne email hvis du har nogen spørgsmål.';
