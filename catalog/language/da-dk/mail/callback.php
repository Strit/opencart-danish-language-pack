<?php
// Text
$_['text_subject']		= '%s - Ring tilbage ordrer';
$_['text_waiting']		= 'Opkald Venter.';
$_['text_telephone']	= 'Telefon: %s';
$_['text_name']			= 'Navn: %s';
$_['text_enquiry']		= 'Kommentar: %s';
$_['text_calltime']		= 'Bedste tid for opkald: %s';
