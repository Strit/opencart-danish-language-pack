<?php
// Text
$_['text_subject']      = '%s - Ordre %s';
$_['text_received']     = 'Du har modtaget en ordre.';
$_['text_order_id']     = 'Ordre ID:';
$_['text_date_added']   = 'Dato Tilføjet:';
$_['text_order_status'] = 'Ordre Status:';
$_['text_product']      = 'Produkter';
$_['text_total']        = 'Total';
$_['text_comment']      = 'Kommentarerne til din ordre er:';
