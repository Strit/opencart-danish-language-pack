<?php
// Text
$_['text_subject']  = '%s - Nulstilling af adgangskode forespørgsel';
$_['text_greeting'] = 'En ny adgangskode er forespurgt til %s kunde konto.';
$_['text_change']   = 'For at nulstille din adgangskode skal du klikke på linket herunder:';
$_['text_ip']       = 'IP\'en der er brugt til at lave denne forespørgsel var:';
