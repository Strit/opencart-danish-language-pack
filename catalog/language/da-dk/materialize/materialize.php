<?php
$_['text_email_error']				= 'Forkert email';
$_['text_email_success']			= 'Gyldig email';

// Months Full
$_['text_months_full_january']		= 'Januar';
$_['text_months_full_february']		= 'Februar';
$_['text_months_full_march']		= 'Marts';
$_['text_months_full_april']		= 'April';
$_['text_months_full_may']			= 'Maj';
$_['text_months_full_june']			= 'Juni';
$_['text_months_full_july']			= 'Juli';
$_['text_months_full_august']		= 'August';
$_['text_months_full_september']	= 'September';
$_['text_months_full_october']		= 'Oktober';
$_['text_months_full_november']		= 'November';
$_['text_months_full_december']		= 'December';

// Months Short
$_['text_months_short_january']		= 'Jan';
$_['text_months_short_february']	= 'Feb';
$_['text_months_short_march']		= 'Mar';
$_['text_months_short_april']		= 'Apr';
$_['text_months_short_may']			= 'Maj';
$_['text_months_short_june']		= 'Jun';
$_['text_months_short_july']		= 'Jul';
$_['text_months_short_august']		= 'Aug';
$_['text_months_short_september']	= 'Sep';
$_['text_months_short_october']		= 'Okt';
$_['text_months_short_november']	= 'Nov';
$_['text_months_short_december']	= 'Dec';

// Weekdays Full
$_['text_weekdays_full_sunday']		= 'Søndag';
$_['text_weekdays_full_monday']		= 'Mandag';
$_['text_weekdays_full_tuesday']	= 'Tirsdag';
$_['text_weekdays_full_wednesday']	= 'Onsdag';
$_['text_weekdays_full_thursday']	= 'Torsdag';
$_['text_weekdays_full_friday']		= 'Fredag';
$_['text_weekdays_full_saturday']	= 'Lørdag';

// Weekdays Short
$_['text_weekdays_short_sunday']	= 'Søn';
$_['text_weekdays_short_monday']	= 'Man';
$_['text_weekdays_short_tuesday']	= 'Tir';
$_['text_weekdays_short_wednesday']	= 'Ons';
$_['text_weekdays_short_thursday']	= 'Tor';
$_['text_weekdays_short_friday']	= 'Fre';
$_['text_weekdays_short_saturday']	= 'Lør';

// Buttons
$_['button_date_today']				= 'Idag';
$_['button_time_done']				= 'Vælg';
$_['button_datetime_clear']			= 'Ryd';
$_['button_datetime_done']			= 'Luk';

// Settings
$_['twelve_hour']					= 'false';
$_['first_day']						= 1;
