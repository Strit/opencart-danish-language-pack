<?php
// Heading
$_['heading_title']				= 'Forfatter Liste';

// Text
$_['text_index']				= 'Alfabetisk indeks';
$_['text_refine']				= 'Udvidet Søgning';
$_['text_post']					= 'Indlæg';
$_['text_error']				= 'Kategori ikke fundet!';
$_['text_empty']				= 'Der er ingen indlæg i denne kategori';
$_['text_sort']					= 'Sortér';
$_['text_default']				= 'Standard';
$_['text_name_asc']				= 'Navn (A - Å)';
$_['text_name_desc']			= 'Navn (Å - A)';
$_['text_limit']				= 'Vis';
$_['text_read_more']			= 'Læs Mere';
$_['text_author']				= 'Forfatter';
$_['text_authors']				= 'Forfattere';
$_['text_published']			= 'Udgivelsesdato';
$_['text_view_all_posts']		= 'Se alle indlæg';
$_['text_authors_starting']		= 'Alle forfattere der starter med';
