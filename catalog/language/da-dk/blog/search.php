<?php
// Heading
$_['heading_title']		= 'Søg';
$_['heading_tag']		= 'Tag - ';

// Text
$_['text_search']		= 'Indlæg der passer til søgningen';
$_['text_searched']		= 'Du søgte efter';
$_['text_refine']		= 'Udvidet kategori';
$_['text_keyword']		= 'Nøgleord';
$_['text_category']		= 'Alle kategorier';
$_['text_sub_category']	= 'Søg i alle underkategorier';
$_['text_empty']		= 'Der er ingen indlæg der passer til din søgning.';
$_['text_author']		= 'Forfatter';
$_['text_sort']			= 'Sortér Efter';
$_['text_default']		= 'Standard';
$_['text_name_asc']		= 'Navn (A - Å)';
$_['text_name_desc']	= 'Navn (Å - A)';
$_['text_limit']		= 'Vis';
$_['text_read_more']	= 'Læs mere';
$_['text_author']		= 'Forfatter';
$_['text_published']	= 'Udgivelsesdato';

// Entry
$_['entry_search']		= 'Søgekriterier';
$_['entry_description']	= 'Søg i indlægsbeskrivelser';
