<?php
// Text
$_['text_blog']						= 'Blog';
$_['text_success']					= 'Tak for din kommentar. Den er sendt til moderation.';
$_['text_error']					= 'Side ikke fundet!';
$_['text_author']					= 'Forfatter';
$_['text_published']				= 'Udgivelsesdato';
$_['text_url']						= 'Link til udgivelsen';
$_['text_copy']						= 'Kopiér';
$_['text_no_comments']				= 'Dette indlæg har ingen kommentarer.';
$_['text_write']					= 'Skriv en kommentar';
$_['text_note']						= 'Note: HTML er ikke oversat!';
$_['text_comment']					= 'Hvad synes du?';
$_['text_tags']						= 'Tags';
$_['text_email_error']				= 'Forkert email';
$_['text_email_success']			= 'Emailen er gyldig';
$_['text_сlipboard_error']			= 'Der skete en fejl ved kopiering!';
$_['text_сlipboard_succeess']		= 'Link kopieret!';
$_['text_read_more']				= 'Læs mere';
$_['text_related']					= 'Det kan være interessant';
$_['text_search']					= 'Søg';

// Button
$_['button_pswp_close']				= 'Luk (Esc)';
$_['button_pswp_toggle_fullscreen']	= 'Fuldskærm';
$_['button_pswp_zoom']				= 'Zoom ind/ud';
$_['button_pswp_prev']				= 'Tidligere (venstre pil)';
$_['button_pswp_next']				= 'Næste (højre pil)';
$_['button_share']					= 'Del';

// Help
$_['help_email']					= 'Din email adresse vil ikke blive udgivet!';

//Entry
$_['entry_name']					= 'Dit navn';
$_['entry_email']					= 'Din email adresse';
$_['entry_comment']					= 'Din kommentar';

// Error
$_['error_name']					= 'Navnet skal være mellem 3 og 25 tegn!';
$_['error_email']					= 'Email er ikke korrekt!';
$_['error_text']					= 'Kommentarteksten skal være mellem 25 og 1000 tegn!';
$_['error_captcha']					= 'Captcta ikke klaret';

// Mail
$_['text_subject']					= '%s - Indlægskommentarer';
$_['text_waiting']					= 'En ny kommentar til indlægget venter.';
$_['text_post']						= 'Indlæg: %s';
$_['text_commentator']				= 'Kommentator: %s';
$_['text_email']					= 'Email: %s';
$_['text_comment']					= 'Kommentartekst:';
