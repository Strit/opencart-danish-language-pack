<?php
// Text
$_['text_success'] = 'Success: API session er startet!';

// Error
$_['error_key']    = 'Advarsel: Forkert API nøgle!';
$_['error_ip']     = 'Advarsel: Din IP %s har ikke adgnag til denne API!';
