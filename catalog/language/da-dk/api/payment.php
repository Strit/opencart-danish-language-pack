<?php
// Text
$_['text_address']       = 'Success: Betalingsadresse er sat!';
$_['text_method']        = 'Success: Betalingsmetode er sat!';

// Error
$_['error_permission']   = 'Advarsel: Du har ikke rettigheder til at tilgå denne API!';
$_['error_firstname']    = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']     = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_address_1']    = 'Adresse 1 skal være mellem3 og 128 tegn!';
$_['error_city']         = 'By skal være mellem 3 og 128 tegn!';
$_['error_postcode']     = 'Postnummer skal være mellem 2 og 10 tegn for dette land!';
$_['error_country']      = 'Vælg venligst et land!';
$_['error_zone']         = 'Vælg venligst en region / stat!';
$_['error_custom_field'] = '%s påkrævet!';
$_['error_address']      = 'Advarsel: Betalingsadresse påkrævet!';
$_['error_method']       = 'Advarsel: Betalingsmetode påkrævet!';
$_['error_no_payment']   = 'Advarsel: Ingen betalingsmuligheder er tilgængelige!';
