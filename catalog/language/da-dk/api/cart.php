<?php
// Text
$_['text_success']     = 'Success: Du har ændret din indkøbskurv!';

// Error
$_['error_permission'] = 'Advarsel: Du har ikke rettigheder til at tilgå denne API!';
$_['error_stock']      = 'Produkter markeret med *** er ikke tilgængelige i den ænskede mængde eller er ikke på lager!';
$_['error_minimum']    = 'Minimum ordre mængde for %s er %s!';
$_['error_store']      = 'Produkt kan ikke købes fra den valgte butik!';
$_['error_required']   = '%s påkrævet!';
