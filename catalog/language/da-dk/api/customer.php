<?php
// Text
$_['text_success']       = 'Du har ændret kunder';

// Error
$_['error_permission']   = 'Advarsel: Du har ikke rettigheder til at tilgå denne API!';
$_['error_customer']     = 'Du skal første vælge en kunde!';
$_['error_firstname']    = 'Fornavn skal være mellem 1 og 32 tegn!';
$_['error_lastname']     = 'Efternavn skal være mellem 1 og 32 tegn!';
$_['error_email']        = 'Email adresse ser ikke ud til at være gyldig!';
$_['error_telephone']    = 'Telefonnummer skal være mellem 3 og 32 tegn!';
$_['error_custom_field'] = '%s påkrævet!';
