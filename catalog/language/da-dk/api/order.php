<?php
// Text
$_['text_success']           = 'Success: Du har ændret ordrer!';

// Error
$_['error_permission']       = 'Advarsel: Du har ikke rettigheder til at tilgå denne API!';
$_['error_customer']         = 'Advarsel: Kunde detaljer skal være sat!';
$_['error_payment_address']  = 'Advarsel: Betalingsadresse påkrævet!';
$_['error_payment_method']   = 'Advarsel: Betalingsmetode påkrævet!';
$_['error_no_payment']       = 'Advarsel: Ingen betalingsmuligheder tilgængelige!';
$_['error_shipping_address'] = 'Advarsel: Leveringsadresse påkrævet!';
$_['error_shipping_method']  = 'Advarsel: Leveringsmetode påkrævet!';
$_['error_no_shipping']      = 'Advarsel: Ingen levelingsmuligheder tilgængelige!';
$_['error_stock']            = 'Advarsel: Produkter markeret med *** er ikke tilgængelige i den ønskede mængde eller er ikke på lager!';
$_['error_minimum']          = 'Advarsel: Minimum ordre mængde for %s er %s!';
$_['error_not_found']        = 'Advarsel: Ordre kunne ikke findes!';
