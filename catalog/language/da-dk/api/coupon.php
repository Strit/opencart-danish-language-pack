<?php
// Text
$_['text_success']     = 'Success: Din rabat kupon er angivet!';

// Error
$_['error_permission'] = 'Advarsel: Du har ikke rettigheder til at tilgå denne API!';
$_['error_coupon']     = 'Advarsel: Kupon er enten ugyldig, udløbet eller overskredet brugsgrænsen!';
