<?php
// Text
$_['text_success']     = 'Success: Dine point rabat er angivet!';

// Error
$_['error_permission'] = 'Advarsel: Du har ikke rettigheder til at tilgå denne API!';
$_['error_reward']     = 'Advarsel: Skriv venligst antallet af point der skal bruges!';
$_['error_points']     = 'Advarsel: Du har ikke %s point!';
$_['error_maximum']    = 'Advarsel: Det maksimale antal point der kan bruges er %s!';
